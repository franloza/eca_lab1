#include <stdio.h>

#define IMG_HEIGHT 100
#define IMG_WIDTH 100

int main (void)
{
	FILE * fInput;
	FILE * fOutput;

	unsigned char ucInputImage[IMG_HEIGHT][IMG_WIDTH];
	unsigned char ucOutputImage[IMG_HEIGHT-2][IMG_WIDTH-2];
	char iWeights[3][3] = {{1,2,1},{2,4,2},{1,2,1}};
	//char iWeights[3][3] = {{-1,-1,-1},{-1,8,-1},{-1,-1,-1}};
	unsigned char ucBuffer[1000];

	//Read image from file into the buffer
	fInput = fopen("image_grayscale.pgm","rb");	
	for (int i=0; i < 4; i++)	//ignore the header for simplicity, we will assume a fixed image size anyway
		fgets(ucBuffer,1000,fInput);
	fread(ucInputImage,sizeof(char),IMG_HEIGHT*IMG_WIDTH,fInput);
	fclose(fInput);

	//convolution, ignoring the borders
	for (int i = 1; i < IMG_HEIGHT-1; i++)
		for (int j = 1; j < IMG_WIDTH-1; j++)
		{
			int iSum = 0;

			for (int k=-1; k < 2; k++)
				for (int l=-1; l < 2; l++)
					iSum += ((int)(ucInputImage[i+k][j+l]) * (int)iWeights[k+1][l+1]) >> 4;					

			//if (iSum > 255) iSum=255;
			//if (iSum < 0) iSum=0;

			ucOutputImage[i-1][j-1] = (unsigned char)(iSum);	
		}

	//Write result to the output file
	fOutput = fopen("image_grayscale_out.pgm","wb");
	fprintf(fOutput, "P5\n# created by the edge detect application\n%d %d\n255\n",IMG_WIDTH-2, IMG_HEIGHT-2);
	fwrite(ucOutputImage,sizeof(char),(IMG_HEIGHT-2)*(IMG_WIDTH-2),fOutput);
	fclose(fOutput);

	return 0;
}