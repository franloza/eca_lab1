| id_abu                                   | id_abu_2                            | id_rf_1                                          | id_rf_2                                                | id_lsu                                   | imm                                       | id_alu                                         | id_mul                                         |
|                                          |                                     |                                                  |                                                        |                                          |                                           |                                                |                                                |
| .text                                    | .text                               | .text                                            | .text                                                  | .text                                    | .text                                     | .text                                          | .text                                          |
|                                          |                                     |                                                  |                                                        |                                          |                                           |                                                |                                                |
| nop                                      | nop                                 | nop                                              | nop                                                    | nop                                      | nopi                                      | nop                                            | nop    ; always leave this line with only NOPs |
| nop                                      | nop                                 | nop                                              | nop                                                    | nop                                      | imm 3 ; window_width                      | nop                                            | nop                                            |
| nop                                      | nop                                 | srm r12, in0 ; store window_width in r12         | nop                                                    | nop                                      | imm 100 ; img_width                       | nop                                            | nop                                            |
| nop                                      | nop                                 | srm r1, in0 ;store img_width in r1               | nop                                                    | nop                                      | imm 1 ; weight[0][0] ; initial value of i | nop                                            | nop                                            |
| nop                                      | nop                                 | srm r11, in0 ; set i to initial value in r11     | srm r1, in0 ; store weight[0][0] in r1                 | srm r2, in0 ; stride load operations     | imm 2 ; weight[0][1]                      | nop                                            | nop                                            |
| nop                                      | nop                                 | nop                                              | srm r2, in0 ; store weight[0][1] in r2                 | nop                                      | imm 1 ; weight[0][2]                      | nop                                            | nop                                            |
| nop                                      | nop                                 | nop                                              | srm r3, in0 ; store weight[0][2] in r3                 | srm r5, in0 ; stride store operations    | imm 2 ; weight[1][0]                      | nop                                            | nop                                            |
| nop                                      | nop                                 | nop                                              | srm r4, in0 ; store weight[1][0] in r4                 | nop                                      | imm 4 ; weight[1][1]                      | nop                                            | nop                                            |
| nop                                      | nop                                 | nop                                              | srm r5, in0 ; store weight[1][1] in r5                 | nop                                      | imm 2 ; weight[1][2]                      | nop                                            | nop                                            |
| nop                                      | nop                                 | nop                                              | srm r6, in0 ; store weight[1][2] in r6                 | nop                                      | imm 1 ; weight[2][0] ; window end pos     | nop                                            | nop                                            |
| nop                                      | nop                                 | srm r14, in0 ; store window end pos (wep) in r14 | srm r7, in0 ; store weight[2][0] in r7                 | nop                                      | imm 2 ; weight[2][1]                      | nop                                            | nop                                            |
| nop                                      | nop                                 | nop                                              | srm r8, in0 ; store weight[2][1] in r8                 | nop                                      | imm 1 ; weight[2][2] ; increment value    | nop                                            | nop                                            |
| nop                                      | nop                                 | srm r9, in0 ; store increment value in r9        | srm r9, in0 ; store weight[2][2] in r9                 | nop                                      | imm 98 ;img_width-2                       | nop                                            | nop                                            |
| nop                                      | nop                                 | nop                                              | srm r15, in0 ; store img_width-2 in r15                | nop                                      | nopi                                      | nop                                            | nop                                            |
| nop ;branch target i-loop (14)           | nop                                 | nop                                              | nop                                                    | nop                                      | imm 1 ; initial value of j                | nop                                            | nop                                            |
| nop                                      | nop                                 | srm r10, in0 ; set j to initial value in r10     | nop                                                    | nop                                      | imm 16286 ;initial addr: 16285 + 1        | nop                                            | nop                                            |
| nop                                      | nop                                 | lrm_srm r11, r2, in0 ; load i; store 16826 in r2 | lrm r15 ; load img_width -2                            | nop                                      | nopi                                      | nop                                            | mullu out0, in0, in1 ; i*(img_width-2)         |
| nop                                      | nop                                 | lrm r2 ; load 16826                              | nop                                                    | nop                                      | nopi                                      | add out1, in0, in2 ; 16286 +98*i               | nop                                            |
| nop                                      | nop                                 | nop                                              | nop                                                    | srm r13, in3 ;store init.address (store) | nopi                                      | nop                                            | nop                                            |
| nop ;branch target j-loop (19)           | nop                                 | nop                                              | nop                                                    | nop                                      | imm 0 ; initial value sum                 | nop                                            | nop                                            |
| nop                                      | srm r2, in0 ; init sum              | nop                                              | nop                                                    | nop                                      | imm -1 ; initial value of k               | nop                                            | nop                                            |
| nop                                      | srm r1, in1 ; init reg.number       | lrm r9 ; load 1 in abu_2                         | srm r13, in0 ; set k to initial value in r13           | nop                                      | nopi                                      | nop                                            | nop                                            |
| nop ;branch target k-loop (22)           | nop                                 | nop                                              | nop                                                    | nop                                      | imm -1 ; initial value of l               | nop                                            | nop                                            |
| nop                                      | nop                                 | nop                                              | srm r12, in0 ; set l to initial value                  | nop                                      | nopi                                      | nop                                            | nop                                            |
| nop                                      | nop                                 | lrm r11 ; load i                                 | lrm r13 ; load k                                       | nop                                      | nopi                                      | add out1, in0, in1 ; i+k for init.address      | nop                                            |
| nop                                      | nop                                 | lrm r1 ; load img_width                          | nop                                                    | nop                                      | nopi                                      | nop                                            | mullu out0, in0, in2 ; img_width * (i+k)       |
| nop                                      | nop                                 | lrm r10 ; load j                                 | lrm_srm r12, r11, in3 ; load l, store (i+k)*100 in r11 | nop                                      | nopi                                      | add out1, in0, in1 ; j-1 for init. address     | nop                                            |
| nop                                      | nop                                 | nop                                              | lrm r11 ; load store (i+k)*100                         | nop                                      | nopi                                      | add out1, in1, in3 ; 100*(i+k)+(j-1)           | nop                                            |
| nop                                      | nop                                 | nop                                              | nop                                                    | srm r9, in3 ;store init.address (load)   | nopi                                      | nop                                            | nop                                            |
| nop ;branch target l-loop (29)           | lrm r1 ; load reg.number            | nop                                              | nop                                                    | lgi BYTE, out1 ; load pixel              | nopi                                      | nop                                            | nop                                            |
| nop                                      | nop                                 | nop                                              | lra in1 ; load indexed register (weight)               | nop                                      | nopi                                      | nop                                            | mullu out0, in1, in3 ; pixel * weight          |
| nop                                      | accu r1, in1 ; reg.number++         | lrm r9 ; load increment value                    | nop                                                    | nop                                      | nopi                                      | shra4 out1, in2 ; (weight * pixel in mul) >> 4 | nop                                            |
| nop                                      | accs r2, in3 ; sum += weighed pixel | nop                                              | nop                                                    | nop                                      | nopi                                      | nop                                            | nop                                            |
| nop                                      | lrm r2 ; load sum                   | lrm r14 ; load wep                               | lrm r12 ; load l                                       | nop                                      | nopi                                      | neq out0, in0, in1 ; check if l != wep         | nop                                            |
| bcai 29, in0 ; if l != wep PC=29         | nop                                 | lrm r9 ; load increment value                    | lrm r12 ; load l                                       | nop                                      | nopi                                      | add out1, in0, in1 ; l++                       | nop                                            |
| nop ; branch delay slot 1                | nop                                 | nop                                              | srm r12, in2 ; store new value of l                    | nop                                      | nopi                                      | nop                                            | nop                                            |
| nop ; branch delay slot 2                | nop                                 | nop                                              | nop                                                    | nop                                      | nopi                                      | nop                                            | nop                                            |
| nop                                      | nop                                 | lrm r14 ; load wep                               | lrm r13 ; load k                                       | nop                                      | nopi                                      | neq out0, in0, in1 ; check if k != wep         | nop                                            |
| bcai 22, in0 ; if k != wep PC=18         | nop                                 | lrm r9 ; load increment value                    | lrm r13 ; load k                                       | nop                                      | nopi                                      | add out1, in0, in1 ; k++                       | nop                                            |
| nop ; branch delay slot 1                | nop                                 | nop                                              | srm r13, in2 ; store new value of k                    | nop                                      | nopi                                      | nop                                            | nop                                            |
| nop ; branch delay slot 2                | nop                                 | nop                                              | nop                                                    | nop                                      | nopi                                      | nop                                            | nop                                            |
| nop                                      | nop                                 | nop                                              | nop                                                    | sgi BYTE, in2 ; write sum                | imm 1 ; increment value                   | nop                                            | nop                                            |
| nop                                      | nop                                 | lrm r10 ; load j                                 | lrm_srm r15, r10, in0 ; load img_width-2, store inc.   | nop                                      | nopi                                      | neq out0, in0, in1 ; check if j != IMG_WIDTH-2 | nop                                            |
| bcai 19, in0 ; if j != IMG_WIDTH-2 PC=19 | nop                                 | lrm r10 ; load j                                 | lrm r10 ; load inc. value                              | nop                                      | nopi                                      | add out1, in0, in1 ; j++                       | nop                                            |
| nop ; branch delay slot 1                | nop                                 | srm r10, in2 ; store new value of j              | nop                                                    | nop                                      | nopi                                      | nop                                            | nop                                            |
| nop ; branch delay slot 2                | nop                                 | nop                                              | nop                                                    | nop                                      | nopi                                      | nop                                            | nop                                            |
| nop                                      | nop                                 | lrm r11 ; load i                                 | lrm r15 ; load img_width-2                             | nop                                      | nopi                                      | neq out0, in0, in1 ; check if j != IMG_WIDTH-2 | nop                                            |
| bcai 14, in0 ; if i != IMG_WIDTH-2 PC=14 | nop                                 | lrm r11 ; load i                                 | lrm r10 ; load inc. value                              | nop                                      | nopi                                      | add out1, in0, in1 ; i++                       | nop                                            |
| nop ; branch delay slot 1                | nop                                 | srm r11, in2 ; store new value of i              | nop                                                    | nop                                      | nopi                                      | nop                                            | nop                                            |
| nop ; branch delay slot 2                | nop                                 | nop                                              | nop                                                    | nop                                      | nopi                                      | nop                                            | nop                                            |
| jai 0 ; terminate execution              | nop                                 | nop                                              | nop                                                    | nop                                      | nopi                                      | nop                                            | nop                                            |
| nop                                      | nop                                 | nop                                              | nop                                                    | nop                                      | nopi                                      | nop                                            | nop                                            |













































































